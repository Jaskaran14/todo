import { useStateContext } from "../StateProvider";

export default function DeleteConfirmation() {
  const { edTask, setEdTask, taskArr, setTaskArr } = useStateContext();

  // delete the task from the table and from localstorage too.
  const cancelDelete = () => {
    setEdTask({});
  };

  const deleteTask = () => {
    let copiedArr = [...taskArr];
    copiedArr.splice(edTask.index, 1);
    setTaskArr(copiedArr);
    localStorage.setItem("todoList", JSON.stringify(copiedArr));
    setEdTask({});
  };

  return (
    <div id="delete-task" className="modal" tabIndex="-1">
      <div className="modal-dialog">
        <div className="modal-content">
          <div className="modal-header">
            <h5 className="modal-title">Confirm Delete</h5>
            <button
              type="button"
              className="btn-close"
              data-bs-dismiss="modal"
              aria-label="Close"
              onClick={cancelDelete}
            ></button>
          </div>
          <div className="modal-body">
            <p>Are you sure you want to delete the task?</p>
          </div>
          <div className="modal-footer">
            <button
              type="button"
              className="btn btn-secondary"
              data-bs-dismiss="modal"
              onClick={cancelDelete}
            >
              Cancel
            </button>
            <button
              type="button"
              className="btn btn-outline-secondary"
              data-bs-dismiss="modal"
              onClick={deleteTask}
            >
              Confirm
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}
