import { useStateContext } from "../StateProvider";
import { useState } from "react";
export default function Searchbar() {
  const [searchInput, setSearchInput] = useState("");
  const { setSearchedValue, searchedTask } = useStateContext();
  const handleSearch = () => {
    setSearchedValue(searchInput.trim());
    searchedTask();
  };
  return (
    <div className="d-flex my-3">
      <input
        type="text"
        id="search-input"
        className="form-control"
        placeholder="Search here..."
        value={searchInput}
        onChange={(e) => setSearchInput(e.target.value)}
      />
      <button
        id="search-btn"
        className="btn btn-outline-secondary mx-2"
        type="button"
        onClick={() => handleSearch()}
      >
        Search
      </button>
    </div>
  );
}
