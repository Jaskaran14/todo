import { useState, useEffect } from "react";
import { useStateContext } from "../StateProvider";
export default function TaskModal() {
  const [title, setTitle] = useState("");
  const [desc, setDesc] = useState("");
  const { edTask, setEdTask, handleSave } = useStateContext();

  const cancel = () => {
    setTitle("");
    setDesc("");
    setEdTask({});
  };

  useEffect(() => {
    if (edTask && edTask.task) {
      setTitle(edTask.task.title);
      setDesc(edTask.task.desc);
    } else {
      setTitle("");
      setDesc("");
    }
  }, [edTask]);

  const isInputFill = title?.trim() && desc?.trim();
  const handleSubmit = (e) => {
    e.preventDefault();
    if (isInputFill) {
      handleSave({ title, desc });
      setTitle("");
      setDesc("");
    }
  };

  return (
    <div
      id="task-modal"
      className="modal fade"
      tabIndex="-1"
      aria-labelledby="TaskModal"
      aria-hidden="true"
    >
      <div className="modal-dialog">
        <div className="modal-content">
          <form onSubmit={handleSubmit}>
            <div className="modal-header">
              <h1 className="modal-title fs-5" id="addModal">
                Todo
              </h1>
              <button
                type="button"
                className="btn-close"
                data-bs-dismiss="modal"
                aria-label="Close"
                onClick={cancel}
              ></button>
            </div>
            <div className="modal-body">
              <div className="mb-3">
                <label htmlFor="task-title" className="col-form-label">
                  Title:
                </label>
                <input
                  type="text"
                  className="form-control"
                  id="task-title"
                  placeholder="Add title here..."
                  value={title}
                  onChange={(e) => {
                    setTitle(e.target.value);
                  }}
                  required
                />
              </div>
              <div className="mb-3">
                <label htmlFor="task-description" className="col-form-label">
                  Description:
                </label>
                <textarea
                  className="form-control"
                  id="task-description"
                  placeholder="Enter Description of the task here..."
                  value={desc}
                  onChange={(e) => {
                    setDesc(e.target.value);
                  }}
                  required
                ></textarea>
              </div>
            </div>

            <div className="modal-footer">
              <button
                id="cancel-btn"
                type="button"
                className="btn btn-secondary"
                data-bs-dismiss="modal"
                onClick={cancel}
              >
                Cancel
              </button>
              <button
                id="add-btn"
                type="submit"
                className="btn btn-outline-secondary"
                data-bs-dismiss={isInputFill ? "modal" : ""}
              >
                Save
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}
