export default function AddButton() {
  return (
    <div
      id="add-logo"
      className="add bg-secondary d-flex justify-content-center align-items-center"
      data-bs-toggle="modal"
      data-bs-target="#task-modal"
    >
      <i className="uil uil-plus"></i>
    </div>
  );
}
