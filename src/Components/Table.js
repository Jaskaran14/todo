import TaskRow from "./TaskRow";
import { useState } from "react";
import { useStateContext } from "../StateProvider";

export default function Table() {
  const { taskArr, searchedValue, searchedTask } = useStateContext();

  const tasks = searchedValue ? searchedTask() : taskArr;

  return (
    <div className="overflow-x-auto">
      <table className="table table-striped table-hover">
        <thead>
          <tr>
            <th scope="col">S.No.</th>
            <th scope="col">Title</th>
            <th scope="col">Description</th>
            <th scope="col">Edit</th>
            <th scope="col">Delete</th>
          </tr>
        </thead>
        <tbody id="table-body">
          {tasks.length === 0 ? (
            <tr>
              <th></th>
              <td></td>
              <td className="d-flex justify-content-center">
                No Tasks Available
              </td>
              <td></td>
              <td></td>
            </tr>
          ) : (
            tasks.map((task, index) => (
              <TaskRow key={index} index={index} task={task} />
            ))
          )}
        </tbody>
      </table>
    </div>
  );
}
