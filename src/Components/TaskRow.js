import { useStateContext } from "../StateProvider";

export default function TaskRow({ index, task }) {
  const { setEdTask } = useStateContext();

  const handleEdClick = () => {
    setEdTask({ index, task });
  };
  return (
    <tr>
      <th scope="row">{index + 1}</th>
      <td>{task.title}</td>
      <td>{task.desc}</td>
      <td>
        <button
          className="btn btn-outline-secondary btn-sm"
          type="button"
          data-bs-toggle="modal"
          data-bs-target="#task-modal"
          onClick={handleEdClick}
        >
          Edit
        </button>
      </td>
      <td>
        <button
          className="btn btn-outline-secondary btn-sm"
          type="button"
          data-bs-toggle="modal"
          data-bs-target="#delete-task"
          onClick={handleEdClick}
        >
          Delete
        </button>
      </td>
    </tr>
  );
}
