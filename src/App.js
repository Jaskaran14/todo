import { StateProvider } from "./StateProvider.js";
import Table from "./Components/Table.js";
import Searchbar from "./Components/SearchBar.js";
import AddButton from "./Components/AddButton.js";
import TaskModal from "./Components/TaskModal.js";
import DeleteConfirmation from "./Components/DeleteConfirmation.js";

export default function App() {
  return (
    <StateProvider>
      <div id="main-container" className="container">
        <h2 className="text-center mt-3 mb-5">TODO's LIST</h2>
        <Searchbar />
        <Table />
        <AddButton />
        <TaskModal />
        <DeleteConfirmation />
      </div>
    </StateProvider>
  );
}
