import React, { createContext, useContext, useState, useEffect } from "react";
const StateContext = createContext();

export const useStateContext = () => useContext(StateContext);

export const StateProvider = ({ children }) => {
  const [taskArr, setTaskArr] = useState([]);
  const [edTask, setEdTask] = useState({});
  const [searchedValue, setSearchedValue] = useState();

  // Add or Edit the task and used in save button in TaskModal
  const handleSave = ({ title, desc }) => {
    const updatedTaskArr = [...taskArr];
    if (
      edTask.index === undefined ||
      edTask.index === null ||
      edTask.index === ""
    ) {
      updatedTaskArr.push({ title, desc });
    } else {
      updatedTaskArr[edTask.index] = { title, desc };
    }

    setTaskArr(updatedTaskArr);
    localStorage.setItem("todoList", JSON.stringify(updatedTaskArr));
    setEdTask({});
  };

  useEffect(() => {
    let todoItems = JSON.parse(localStorage.getItem("todoList"));
    if (todoItems) {
      setTaskArr(todoItems);
    }
  }, []);

  // this function map through the taskArr and return searched task
  const searchedTask = () => {
    return taskArr.filter(
      (task) => searchedValue === task.title || searchedValue === task.desc
    );
  };

  return (
    <StateContext.Provider
      value={{
        taskArr,
        setTaskArr,
        edTask,
        setEdTask,
        handleSave,
        searchedValue,
        setSearchedValue,
        searchedTask,
      }}
    >
      {children}
    </StateContext.Provider>
  );
};
